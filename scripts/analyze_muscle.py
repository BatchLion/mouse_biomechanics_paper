""" Script that plots figures of data for the purpose of analysis """

from argparse import ArgumentParser
from pathlib import Path

import farms_pylog as pylog
import h5py
import matplotlib.pyplot as plt
import numpy as np
import yaml
from tqdm import tqdm

from farms_data.io.yaml import read_yaml
from farms_utils.plotting import PlotType, get_fig_size

#: DATA PATH
FILE_PATH = Path(__file__).parent.absolute()
DATA_PATH = FILE_PATH.joinpath("..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_RESULTS_PATH = DATA_PATH.joinpath("results", "muscle_analysis")

#: Load default plot params
with open(CONFIG_PATH.joinpath("plotting.yaml"), 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)
plot_params["figure.figsize"] = get_fig_size(PlotType.VIEW)
plot_params["text.usetex"] = False
#: Set plot properties
# plt.rcParams.update(plot_params)


def plot_moment_arm(muscle_data):
    """Plot all the moment arms"""
    muscle_name = Path(muscle_data.name).name
    active_joints = muscle_data.attrs['active_joints']
    for joint in active_joints:
        fig, ax = plt.subplots()
        ax.plot(
            np.rad2deg(muscle_data[joint]['primary_joint_angles']),
            np.asarray(muscle_data[joint]['moment_arm'])*1e3
        )
        ax.set_title(f"{muscle_name} Moment Arm")
        ax.set_ylabel("Moment arm [mm]")
        ax.set_xlabel(f"{joint} [deg]")


def analyze_muscle(muscle_name:str, **kwargs):
    """ Analyze muscle from the muscle data
    Parameters
    ----------
    muscle_name: <str>
        Name of the muscle to be analyzed
    kwargs: <dict>
        Keyword arguments to plot specific muscle attributes
    """
    # Read the main data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')
    # Get the muscle data from main data
    for side_data in data.values():
        if side_data.__contains__(muscle_name):
            muscle_data = side_data[muscle_name]
            break
    # Plot attributes
    plot_moment_arm(muscle_data)
    # Show
    plt.show()


def parse_args():
    """Parse arguments for the scripts"""
    parser = ArgumentParser("Muscle analysis")
    parser.add_argument(
        "--muscle", "-m", required=True, dest="muscle_name", type=str
    )
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    analyze_muscle(args.muscle_name)
