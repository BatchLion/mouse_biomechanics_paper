import os
from pathlib import Path

import bpy
import mathutils
from farms_blender.core import materials, muscles, pose, scene, utils
from farms_blender.core.display import display_farms_types
from farms_data.io.yaml import read_yaml
from loading import load_model, load_muscles

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
POSE_CONFIG_PATH = CONFIG_PATH.joinpath("pose")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")
SDF_MODEL_PATH = DATA_PATH.joinpath("models", "sdf")
DEV_BONES_PATH = DATA_PATH.joinpath(
    "models", "forelimb", "ForeLimbAtlas", "stl", "bones"
)
DEV_MUSCLES_PATH = DATA_PATH.joinpath(
    "models", "forelimb", "ForeLimbAtlas", "stl", "muscles_and_tendons"
)


def main():
    """ Main """
    sdf_path = SDF_MODEL_PATH.joinpath("left_forelimb.sdf")
    muscles_config_path = MUSCLE_CONFIG_PATH.joinpath("pqu.yaml")
    #: Clear the world
    utils.clear_world()
    #: Load animat
    model_name, model_objs = load_model(sdf_name=sdf_path)

    #: Elements to display. Can be changed later
    display = {
        'view': True,
        'render': True,
        'link': False,
        'visual': True,
        'collision': False,
        'inertial': False,
        'com': False,
        'muscle': True,
        'joint': False,
        'joint_axis': False,
        'position': False,
        'velocity': False,
        'torque': False,
        'reaction': False,
        'friction': False,
        'hydro_force': False,
        'hydro_torque': False,
    }
    display_farms_types(**display)
    #: Load muscle
    load_muscles(
        model_name, muscle_config_name=muscles_config_path
    )
    #: Set muscle radius
    muscles.set_muscle_radius(radius=0.0001)
    # set pose
    model_pose = read_yaml(
        POSE_CONFIG_PATH.joinpath("dev_forelimb_muscle.yaml")
    )
    pose.set_model_pose(model_pose, units='degrees')
    #:
    display['link'] = False
    display['joint'] = True#
    display_farms_types(**display)

    # : Load the original model for reference
    # SCALE OF TO FIT APPROX = 0.00062
    scale=0.000062
    bpy.ops.import_mesh.stl(
        filepath=str(DEV_BONES_PATH.joinpath("forearm.stl")),
        global_scale=scale
    )

    original = bpy.context.active_object
    original.location = mathutils.Vector(
        (-0.01557117819786072, 0.05636956915259361, 0.07033812813460827))
    original.rotation_euler = mathutils.Euler(
        (-2.9705350399017334, 0.7853981852531433, -1.3089969158172607), 'XYZ')

    #: Load muscle
    bpy.ops.import_mesh.stl(
        filepath=str(DEV_MUSCLES_PATH.joinpath("pronator_quadratus.stl")),
        global_scale=scale
    )

    # muscle = bpy.context.active_object
    # muscle.location = mathutils.Vector(
    #     (-0.01557117819786072, 0.05636956915259361, 0.07033812813460827))
    # muscle.rotation_euler = mathutils.Euler(
    #     (-2.9705350399017334, 0.7853981852531433, -1.3089969158172607), 'XYZ')
    # muscle.active_material = materials.farms_material('force')

    # bpy.ops.import_mesh.stl(
    #     filepath=str(DEV_MUSCLES_PATH.joinpath("triceps_brachii_long.stl")),
    #     global_scale=scale
    # )

    # muscle = bpy.context.active_object
    # muscle.location = mathutils.Vector(
    #     (-0.01557117819786072, 0.05636956915259361, 0.07033812813460827))
    # muscle.rotation_euler = mathutils.Euler(
    #     (-2.9705350399017334, 0.7853981852531433, -1.3089969158172607), 'XYZ')
    # muscle.active_material = materials.farms_material('friction')

    # bpy.ops.import_mesh.stl(
    #     filepath=str(DEV_MUSCLES_PATH.joinpath("triceps_brachii_medial.stl")),
    #     global_scale=scale
    # )

    muscle = bpy.context.active_object
    muscle.location = mathutils.Vector(
        (-0.01557117819786072, 0.05636956915259361, 0.07033812813460827))
    muscle.rotation_euler = mathutils.Euler(
        (-2.9705350399017334, 0.7853981852531433, -1.3089969158172607), 'XYZ')
    muscle.active_material = materials.farms_material('torque')


if __name__ == '__main__':
    main()
