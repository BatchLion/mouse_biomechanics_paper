""" Estimate forelimb hill muscle properties """

import os
import sys
from pathlib import Path

import farms_pylog as pylog
import h5py
import numpy as np
import tqdm

import bpy
from farms_blender.core import muscles
from farms_blender.core.display import display_farms_types
from farms_blender.core.utils import clear_world
from farms_data.io.yaml import read_yaml, write_yaml
from loading import load_model, load_muscles, load_scene

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
CONFIG_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath(
    "muscle_groups_definition.yaml")
RESULTS_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")

# Seed for random number generator

# Original muscle lengths from mathewsons paper
original_muscle_tendon_lengths = {"AN": 0.00405,
                                 "BBL": 0.00856,
                                 "BBS": 0.00950,
                                 "BRA": 0.00553,
                                 "COR": 0.00839,
                                 "ECRB": 0.00767,
                                 "ECRL": 0.00776,
                                 "ECU": 0.00649,
                                 "EIP1": 0.00548,
                                 "EIP2": 0.00548,
                                 "FCR": 0.00752,
                                 "FCU": 0.00819,
                                 "PLO": 0.00596,
                                 "PQU": 0.00435,
                                 "PTE": 0.00663,
                                 "TBL": 0.00937,
                                 "TBM": 0.01093,
                                 "TBO": 0.00819
                                 }

# Original muscle-fiber lengths from mathewsons paper
original_muscle_fiber_lengths = {"AN": 0.00213,
                                 "BBL": 0.00555,
                                 "BBS": 0.00598,
                                 "BRA": 0.00410,
                                 "COR": 0.00430,
                                 "ECRB": 0.00604,
                                 "ECRL": 0.00560,
                                 "ECU": 0.00274,
                                 "EIP1": 0.00378,
                                 "EIP2": 0.00378,
                                 "FCR": 0.00367,
                                 "FCU": 0.00380,
                                 "PLO": 0.00362,
                                 "PQU": 0.00082,
                                 "PTE": 0.00256,
                                 "TBL": 0.00629,
                                 "TBM": 0.00532,
                                 "TBO": 0.0051
                                 }


def compute_model_muscle_tendon_lengths(
        model_name: str, muscle_names: str, muscle_suffix: str="RIGHT_FORE_{}"
)->float:
    """
    Parameters
    ----------
    model_name : <str>
        Name of the model
    muscle_name : <list>
        List of muscle names
    muscle_suffix : <str, "RIGHT_FORE_{}">
        Suffix of the muscle to be used

    Returns
    -------
    out : <float>
        Muscle tendon length

    """
    muscle_tendon_lengths = {
        muscle: np.average(
            [
                np.average(muscles.compute_muscle_tendon_length(
                    muscle_suffix.format(muscle), joint
                )[1])
                for joint in muscles.get_muscle_joints(
                        model_name, muscle_suffix.format(muscle)
                )
            ]
        )
        for muscle in muscle_names
    }
    return muscle_tendon_lengths


def compute_length_ratio(muscle_names, original_lengths, model_lengths):
    """ Compute length ratios for each muscle

    Parameters
    ----------
    original :


    Returns
    -------
    out :

    """
    length_ratios = {
        muscle: model_lengths[muscle]/original_lengths[muscle]
        for muscle in muscle_names
    }
    return length_ratios


def compute_scaled_muscle_fiber_length(muscle_names, original_lengths, length_ratios):
    """ Compute scaled muscle fiber length

    Parameters
    ----------
    original :


    Returns
    -------
    out :

    """
    scaled_lengths = {
        muscle: original_lengths[muscle]*length_ratios[muscle]
        for muscle in muscle_names
    }
    return scaled_lengths


def update_model_muscle_fiber_lengths(muscle_names, model_fiber_lengths:str)-> None:
    """Update model muscle fiber lengths within the blender scene

    Parameters
    ----------
    model_fiber_lengths : <dict>
        Dictionary of muscle name and muscle optimal fiber length

    Returns
    -------
    out : <None>
    """
    for muscle in muscle_names:
        for side in ("LEFT", "RIGHT"):
            muscle_obj = bpy.context.scene.objects[f"{side}_FORE_{muscle}"]
            muscle_obj["l_opt"] = model_fiber_lengths[muscle]


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    clear_world()
    # Load animat
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene()

    # Update display
    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': False,
               'visual': False, 'collision': False, 'inertial': False,
               'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
               }
    display_farms_types(objs=model_objs, **display)
    return model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def update_forelimb_config():
    """Update lts values in the forelimb config file"""
    # data file path
    file_path = MUSCLE_RESULTS_PATH.joinpath("forelimb_lts_estimation.h5")
    data_file = h5py.File(file_path, 'r')
    # Read the config file
    forelimb_config = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("forelimb.yaml")
    )
    # Update the tendon slack lengths
    for name, muscle in data_file.items():
        for side in ("LEFT", "RIGHT"):
            forelimb_config["muscles"][f"{side}_FORE_{name}"]["l_slack"] = float(
                data_file[name]["average_lts"][()]
            )
    # Write updated config
    write_yaml(
        forelimb_config, MUSCLE_CONFIG_PATH.joinpath("forelimb.yaml")
    )


def main():
    """ MAIN """
    # Setup scene
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = setup_scene()
    # Load muscle names
    muscle_names = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("names.yaml")
    )["FORE"]
    muscle_suffix="RIGHT_FORE_{}"
    # Compute model muscle tendon lengths
    model_muscle_tendon_lengths = compute_model_muscle_tendon_lengths(
        model_name, muscle_names, muscle_suffix
    )
    # Compute length ratios
    length_ratios = compute_length_ratio(
        muscle_names, original_muscle_tendon_lengths, model_muscle_tendon_lengths
    )
    # Compute scaled optimal fiber lengths
    model_muscle_fiber_lengths = compute_scaled_muscle_fiber_length(
        muscle_names, original_muscle_fiber_lengths, length_ratios
    )
    # Update computed fiber lengths
    update_model_muscle_fiber_lengths(muscle_names, model_muscle_fiber_lengths)
    # Prepare dataset to store the values
    file_path = MUSCLE_RESULTS_PATH.joinpath("forelimb_lts_estimation.h5")
    data_file = h5py.File(file_path, 'w')
    # Optimization
    for muscle in tqdm.tqdm(muscle_names):
        pylog.debug(f"Evaluating {muscle}")
        muscle_data = data_file.create_group(f"{muscle}")
        # optimize for lts
        res, lts = muscles.optimize_tendon_slack_length_manal(
            model_name, muscle_suffix.format(muscle), is_plot=False,
            debug=False
        )
        # optimization results
        muscle_data.attrs['fun'] = res['fun']
        muscle_data.attrs['message'] = res['message']
        muscle_data.attrs['success'] = res['success']
        muscle_data.attrs['nit'] = res['nit']
        muscle_data.create_dataset(name="x", data=res['x'])
        muscle_data.create_dataset(name="lts", data=lts)
        muscle_data.create_dataset(name="average_lts", data=np.average(lts))
    # Close data file
    data_file.close()


if __name__ == '__main__':
    main()
    update_forelimb_config()
