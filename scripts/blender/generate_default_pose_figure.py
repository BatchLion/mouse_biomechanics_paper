""" Generate default pose figure """
import os
import pathlib
import sys

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse, Rectangle
from scipy.spatial.transform import Rotation
import yaml

import bpy
sys.path.append(bpy.data.filepath)
import mathutils
import mouse_scene
from bpy_extras.object_utils import world_to_camera_view
from farms_models.utils import get_sdf_path
from farms_utils.plotting import get_fig_size, PlotType
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.freestyle import (configure_lineset, create_lineset,
                                          default_lineset_config,
                                          enable_freestyle, remove_lineset,
                                          set_freestyle_setting)
from farms_blender.core.materials import farms_material, flat_shade_material
from farms_blender.core.primitives import create_sphere
from farms_blender.core.pose import set_model_pose
from farms_blender.core.resources import get_resource_object
from farms_blender.core.scene import load_animat, set_scene_settings
from farms_blender.core.sdf import get_base_link, load_sdf
from farms_blender.core.transforms import (blender_global_coordinates,
                                           set_parent)
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.utils import clear_world


def load_mouse(**kwargs):
    """Load mouse model

    Parameters
    ----------
    **kwargs :

    Returns
    -------
    out :

    """
    model_offset = kwargs.pop('model_offset', (0.0, 0.0, 0.0))
    model_name, *_, objs = load_sdf(
        path=get_sdf_path(
                name='mouse',
                version='4',
            ),
        **kwargs
    )
    base_link = get_base_link(model_name)
    base_link.location = model_offset
    return model_name, objs


def configure_scene(**kwargs):
    """ Configure scene. """
    #: Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    #: Enable freestyle
    enable_freestyle()


def configure_freestyle():
    """ Configure line style settings. """
    #: Create collections for visual objects
    for obj, _ in objs_of_farms_types(visual=True):
        add_object_to_collection(obj, collection='visuals', create=True)

    #: Genric settings
    set_freestyle_setting('crease_angle', 0)
    #: Remove default lineset
    remove_lineset('LineSet')
    #: Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["visuals",]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = True
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections['visuals']
        configure_lineset(lineset, **lineset_config)


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1., 0.0, 0.03), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.2
    }
    #: Create camera 0
    camera_link = create_multiview_camera(
        0, camera_options
    )


def main():
    """ main """
    #: Load default scene
    mouse_scene.scene(scale=1e-2)
    #: Configure scene
    configure_scene()
    #: Load model
    model_name, objs = load_mouse(
        model_offset=(0.0, 0.0, 5e-2), resources_scale=5e-3
    )
    #: add cameras
    add_cameras()
    #: Configure freestyle
    configure_freestyle()
    #: Display
    display = {
        'view': True,
        'render': True,
        'link': False,
        'visual': True,
        'collision': False,
        'inertial': False,
        'com': False,
        'muscle': False,
        'joint': False,
        'joint_axis': False,
    }
    display_farms_types(objs=objs, **display)


if __name__ == '__main__':
    main()
