""" Script to generate hindlimb muscles figure. """

import os
import pathlib
import sys

import numpy as np

import bpy
import mouse_scene
from farms_blender.core import camera, muscles, pose, sdf
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.freestyle import (
    configure_lineset, create_lineset, default_lineset_config,
    enable_freestyle, remove_lineset, set_freestyle_setting
)
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.render import auto_crop_image
from farms_blender.core.scene import set_scene_settings
from farms_data.io.yaml import read_yaml
from loading import load_model, load_muscles, load_scene
from mathutils import Euler, Matrix, Vector

# Global config paths
SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
POSE_CONFIG_PATH = CONFIG_PATH.joinpath("pose")
SDF_PATH = DATA_PATH.joinpath("models", "sdf")
FIGURES_PATH = SCRIPT_PATH.joinpath(
    "..", "..", "src", "figures", "muscles", "hindlimb"
)


def configure_scene(**kwargs):
    """ Configure scene. """
    set_scene_settings(renderer='BLENDER_EEVEE')
    # background light
    bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 1.0
    # Render size
    bpy.context.scene.render.resolution_x = 1080
    bpy.context.scene.render.resolution_y = 1920
    # Render samples
    bpy.context.scene.eevee.taa_render_samples = 64
    # Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    # Enable freestyle
    enable_freestyle()


def configure_freestyle():
    """ Configure line style settings. """
    # Line thinckness
    bpy.data.scenes['Scene'].render.line_thickness = 1.0

    # Create collections for visual objects
    for obj, _ in objs_of_farms_types(visual=True, link=True):
        add_object_to_collection(obj, collection='visuals', create=True)

    # Genric settings
    set_freestyle_setting('crease_angle', 0)
    # Remove default lineset
    remove_lineset('LineSet')
    # Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["visuals", ]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = True
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections['visuals']
        configure_lineset(lineset, **lineset_config)


def generate_hindlimb(**kwargs):
    """ Generate hindlimb """
    model_offset = kwargs.pop('model_offset', (0.0, 0.0, 0.0))
    # Load hindlimb sdf
    model_name, model_objs = load_model("right_hindlimb.sdf")

    # Load muscle
    muscle_objs = load_muscles(
        model_name, "right_hindlimb.yaml", attachment_radius=2e-4
    )

    # Set model offset
    base_link = sdf.get_base_link(model_name)
    base_link.location = model_offset

    # Display
    display = {'view': True, 'render': True, 'link': False, 'visual': True,
               'collision': False, 'inertial': False, 'com': False, 'muscle': True,
               'joint': False, 'joint_axis': False,
               }
    display_farms_types(**display)

    # Set pose
    model_pose = read_yaml(
        POSE_CONFIG_PATH.joinpath(
            "neutral_pose_with_joint_constrains.yaml"
        )
    )
    pose.set_model_pose(model_pose, units='degrees')


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1.0, 0.00054, 0.025), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.075
    }
    # Create camera 0
    camera_link = camera.create_multiview_camera(
        0, camera_options
    )
    # Create camera 1
    camera_options["loc"] = (0.0, 1.0, 0.025)
    camera_options["rot"] = (np.pi/2, 0.0, np.pi)
    camera_link = camera.create_multiview_camera(
        1, camera_options
    )


def export_figures(export_path: str):
    """Export figures

    Parameters
    ----------
    export_path : <str>
        File path for exporting the figures
    """
    # Choose camera 0
    bpy.data.scenes['Scene'].camera = bpy.data.objects['Camera_0']
    # render settings
    export_path = str(
        export_path.joinpath("hind_limb_muscles_side_view.png")
    )
    bpy.context.scene.render.filepath = export_path
    bpy.ops.render.render(write_still=1)
    # Auto crop image
    auto_crop_image(export_path)


if __name__ == '__main__':
    mouse_scene.scene(scale=1e-2)

    # Configure scene
    configure_scene()

    # Load model
    generate_hindlimb(model_offset=(0.0, 0.0, 0.05))

    # Configure freestyle
    configure_freestyle()

    # Create cameras
    add_cameras()

    # Export figures
    export_figures(export_path=FIGURES_PATH)
