""" Generate figures for mesh registration. """

import os
import pathlib
import sys
from argparse import ArgumentParser

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import to_rgba

import bpy
from farms_blender.core import camera, duplicate, materials, primitives, utils
from farms_blender.core.resources import get_resource_object, import_resources
from farms_blender.core.scene import set_scene_settings
from farms_blender.core.transforms import set_parent
from mathutils import Euler, Matrix, Vector

#: Define three materials
material_a = materials.create_material(
    name="material_a", color=to_rgba('paleturquoise', alpha=1.)
)
material_b = materials.create_material(
    name="material_b", color=to_rgba('plum', alpha=1.)
)

SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()

#: Paths
models_path = os.path.join(
    SCRIPT_PATH, "..", "..", "data", "models"
)
figures_path = os.path.join(
    SCRIPT_PATH, "..", "..", "figures"
)


def duplicate_object(obj):
    obj_dup = obj.copy()
    obj_dup.name = "{}_duplicate".format(obj.name)
    bpy.context.collection.objects.link(obj_dup)
    return obj_dup


def create_new_scene(name: str) -> None:
    """ Create a new scene. """
    scene = bpy.data.scenes.new(name)
    #: Add a world property for the scene
    scene.world = bpy.data.worlds['World']
    return scene


def add_lights():
    """ Add lights """
    #: Add sun
    sun = bpy.data.lights.new(name="sun", type='SUN')
    sun.energy = 2.
    sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
    bpy.context.collection.objects.link(sun_obj)
    #: Change background light_add
    bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 2.0


def generate_pelvis(render=False):
    """
    Generate pelvis mesh registration
    """

    #: Rename scene
    bpy.context.scene.name = "Pelvis"

    #: Clear scene
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=False)

    #: Load HBP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/hbp/RPelvis.stl"
    )
    hbp_obj = bpy.context.active_object
    hbp_obj.name = "pelvis_hbp"
    hbp_obj.active_material = material_a

    #: Duplicate object
    hbp_obj_dup = duplicate_object(hbp_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/hbp/RPelvis.txt"
    )

    point_materials = [
        materials.create_material(
            name="point_{}".format(point_color),
            color=to_rgba(point_color),
            specular=0,
            metallic=0,
            roughness=1,
        ) for point_color in ['r', 'g', 'c', 'm', 'y', 'b']
    ]

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = Vector(
            [0.005700717544555664, 0.006080986714363098, 0.0004686949282884598]) + Vector(point)
        set_parent(obj, hbp_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        obj_dup.location = Vector(
            [0.005700717544555664, 0.006080986714363098, 0.0004686949282884598]) + Vector(point)
        set_parent(obj_dup, hbp_obj_dup)

    #: Move the mesh to zero
    hbp_obj.location -= Vector(
        [0.005700717544555664, 0.006080986714363098, 0.0004686949282884598]
    )
    bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
    hbp_obj.location += Vector([0.1, 0.0, 0.0])
    hbp_obj.rotation_euler = Vector([0., 0., -np.pi/2])
    pelvis_hbp_link = get_resource_object(
        resource_object='link', name="hbp"
    )
    pelvis_hbp_link.scale = (0.001, 0.001, 0.001)

    #: Load NRP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/opensim/RPelvis.stl"
    )
    osim_obj = bpy.context.active_object
    osim_obj.name = "pelvis_osim"
    osim_obj.active_material = material_b

    #: Duplicate object
    osim_obj_dup = duplicate_object(osim_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/opensim/RPelvis.txt")

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = point
        set_parent(obj, osim_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        set_parent(obj_dup, osim_obj_dup)

    osim_obj.location += Vector([-0.1, 0.0, 0.0])
    osim_obj.rotation_euler = Vector([np.pi/2, 0, 0])

    # Load transformation matrix
    transform_matrix = np.loadtxt(
        "../../data/hind_limb_mesh_registration/RPelvisTransformation.txt"
    )
    osim_obj_dup.matrix_world = Matrix(transform_matrix)
    osim_obj_dup.location += Vector([0.0, 0.5, 0.0])
    hbp_obj_dup.location += Vector([0.0, 0.5, 0.0])

    #: Create camera
    camera_ortho_scale = 0.025
    camera_osim = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([-0.1, -1.0, 0.0]),
            'rot': camera.look_at(
                Vector([-0.1, -1.0, 0.0]), osim_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_osim.name = "pelvis_osim_view"

    camera_hbp = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([0.1, -1.0, 0.0]),
            'rot': camera.look_at(
                Vector([0.1, -1.0, 0.0]), hbp_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_hbp.name = "pelvis_hbp_view"

    camera_pos = osim_obj_dup.location + Vector([1.0, 0.0, 0.0])
    camera_transform = camera.create_multiview_camera(
        index=1, options={
            'loc': camera_pos,
            'rot': camera.look_at(
                camera_pos, osim_obj_dup.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_transform.name = "pelvis_transform_view"

    camera.remove_default_multiview_cameras()

    # Lights
    # bpy.ops.object.light_add(
    #     type='SUN', align='WORLD'
    # )
    # light_pelvis = bpy.context.active_object
    # light_pelvis.data.angle = np.pi
    # light_pelvis.data.energy = 2.0
    # light_pelvis.data.shadow_soft_size = 0.2
    # light_pelvis.name = "light_pelvis"

    set_scene_settings(renderer='BLENDER_EEVEE')
    #: Add lights
    add_lights()

    #: Render transparent
    bpy.data.scenes["Pelvis"].render.film_transparent = True
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGBA'

    if render:
        #: Switch camera
        bpy.data.scenes["Pelvis"].camera = camera_hbp
        # render settings
        bpy.context.scene.render.filepath = os.path.join(
            figures_path, "pelvis_hbp_mesh_registration.png")
        bpy.ops.render.render(write_still=1)

        #: Switch camera
        bpy.data.scenes["Pelvis"].camera = camera_osim
        # render settings
        bpy.context.scene.render.filepath = os.path.join(
            figures_path, "pelvis_osim_mesh_registration.png")
        bpy.ops.render.render(write_still=1)

        #: Switch camera
        bpy.data.scenes["Pelvis"].camera = camera_transform
        # render settings
        bpy.context.scene.render.filepath = os.path.join(
            figures_path, "pelvis_transform_mesh_registration.png")
        bpy.ops.render.render(write_still=1)


def generate_femur(render=False):
    """Generate  femur mesh registration
    """

    #: Create scene
    femur_scene = create_new_scene("Femur")
    bpy.context.window.scene = femur_scene

    #: Clear scene
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=False)

    #: Load HBP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/hbp/RFemur.stl"
    )
    hbp_obj = bpy.context.active_object
    hbp_obj.name = "femur_hbp"
    hbp_obj.active_material = material_a

    #: Duplicate object
    hbp_obj_dup = duplicate_object(hbp_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/hbp/RFemur.txt"
    )

    point_materials = [
        materials.create_material(
            name="point_{}".format(point_color),
            color=to_rgba(point_color),
            specular=0,
            metallic=0,
            roughness=1,
        ) for point_color in ['r', 'g', 'c', 'm', 'y', 'b']
    ]

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = Vector(point)
        set_parent(obj, hbp_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        obj_dup.location = Vector(point)
        set_parent(obj_dup, hbp_obj_dup)

    #: Move the mesh to zero
    hbp_obj.location += Vector([0.1, 0.0, 0.0])
    hbp_obj.rotation_euler = Vector([0., 0., 0.0])

    #: Load NRP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/opensim/RFemur.stl"
    )
    osim_obj = bpy.context.active_object
    osim_obj.name = "femur_osim"
    osim_obj.active_material = material_b

    #: Duplicate object
    osim_obj_dup = duplicate_object(osim_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/opensim/RFemur.txt")

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = point
        set_parent(obj, osim_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        set_parent(obj_dup, osim_obj_dup)

    osim_obj.location += Vector([-0.1, 0.0, 0.0])
    osim_obj.rotation_euler = Vector([np.pi/2, 0, np.pi/2])

    # Load transformation matrix
    transform_matrix = np.loadtxt(
        "../../data/hind_limb_mesh_registration/RFemurTransformation.txt"
    )
    osim_obj_dup.matrix_world = Matrix(transform_matrix)
    osim_obj_dup.location += Vector([0.0, 0.5, 0.0])
    hbp_obj_dup.location += Vector([0.0, 0.5, 0.0])
    # hbp_obj_dup.rotation_euler = Vector([0, 0, np.pi/2])

    #: Create camera
    camera_ortho_scale = 0.025
    camera_osim = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([-0.1, -1.0, -0.006]),
            'rot': camera.look_at(
                Vector([-0.1, -1.0, 0.0]), osim_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_osim.data.ortho_scale = camera_ortho_scale
    camera_osim.name = "femur_osim_view"

    camera_hbp = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([0.1, -1.0, -0.0105]),
            'rot': camera.look_at(
                Vector([0.1, -1.0, 0.0]), hbp_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_hbp.name = "femur_hbp_view"

    camera_pos = Vector([0.0, -0.5, -0.0105])
    camera_transform = camera.create_multiview_camera(
        index=1, options={
            'loc': camera_pos,
            'rot': Euler([np.pi/2, 0.0, 0.0]),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_transform.name = "femur_transform_view"

    camera.remove_default_multiview_cameras()

    # Lights

    set_scene_settings(renderer='BLENDER_EEVEE')

    bpy.context.scene.render.resolution_x = 1080
    bpy.context.scene.render.resolution_y = 1920

    #: Add hdri
    add_lights()

    #: Render transparent
    bpy.data.scenes["Femur"].render.film_transparent = True
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGBA'

    #: Switch camera
    bpy.data.scenes["Femur"].camera = camera_hbp
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "femur_hbp_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)

    #: Switch camera
    bpy.data.scenes["Femur"].camera = camera_osim
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "femur_osim_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)

    #: Switch camera
    bpy.data.scenes["Femur"].camera = camera_transform
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "femur_transform_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)


def generate_tibia(render=False):
    """Generate  tibia mesh registration
    """

    #: Create scene
    tibia_scene = create_new_scene("Tibia")
    bpy.context.window.scene = tibia_scene

    #: Clear scene
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=False)

    #: Load HBP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/hbp/RTibia.stl"
    )
    hbp_obj = bpy.context.active_object
    hbp_obj.name = "tibia_hbp"
    hbp_obj.active_material = material_a

    #: Duplicate object
    hbp_obj_dup = duplicate_object(hbp_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/hbp/RTibia.txt"
    )

    point_materials = [
        materials.create_material(
            name="point_{}".format(point_color),
            color=to_rgba(point_color),
            specular=0,
            metallic=0,
            roughness=1,
        ) for point_color in ['r', 'g', 'c', 'm', 'y', 'b']
    ]

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = Vector(point)
        set_parent(obj, hbp_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        obj_dup.location = Vector(point)
        set_parent(obj_dup, hbp_obj_dup)

    #: Move the mesh to zero
    hbp_obj.location += Vector([0.1, 0.0, 0.0])
    hbp_obj.rotation_euler = Vector([0., 0., 0.0])

    #: Load NRP mesh
    bpy.ops.import_mesh.stl(
        filepath="../../data/hind_limb_mesh_registration/opensim/RTibia.stl"
    )
    osim_obj = bpy.context.active_object
    osim_obj.name = "tibia_osim"
    osim_obj.active_material = material_b

    #: Duplicate object
    osim_obj_dup = duplicate_object(osim_obj)

    #: Read text file for points
    ref_points = np.loadtxt(
        "../../data/hind_limb_mesh_registration/opensim/RTibia.txt")

    #: Create spheres and set the positions of the spheres
    for num, point in enumerate(ref_points):
        obj = primitives.create_sphere(0.0003)
        obj.active_material = point_materials[num]
        obj.name = "point_{}".format(num)
        obj.location = point
        set_parent(obj, osim_obj)
        obj_dup = duplicate_object(obj)
        obj_dup.active_material = point_materials[num]
        obj_dup.name = "point_{}_dup".format(num)
        set_parent(obj_dup, osim_obj_dup)

    osim_obj.location += Vector([-0.1, 0.0, 0.0])
    osim_obj.rotation_euler = Vector([np.pi/2, 0, np.pi/2])

    # Load transformation matrix
    transform_matrix = np.loadtxt(
        "../../data/hind_limb_mesh_registration/RTibiaTransformation.txt"
    )
    osim_obj_dup.matrix_world = Matrix(transform_matrix)
    osim_obj_dup.location += Vector([0.0, 0.5, 0.0])
    hbp_obj_dup.location += Vector([0.0, 0.5, 0.0])
    # hbp_obj_dup.rotation_euler = Vector([0, 0, np.pi/2])

    #: Create camera
    camera_ortho_scale = 0.025
    camera_osim = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([-0.1, -1.0, -0.006]),
            'rot': camera.look_at(
                Vector([-0.1, -1.0, 0.0]), osim_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_osim.name = "tibia_osim_view"

    camera_hbp = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([0.1, -1.0, -0.0105]),
            'rot': camera.look_at(
                Vector([0.1, -1.0, 0.0]), hbp_obj.location
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_hbp.name = "tibia_hbp_view"

    camera_pos = Vector([0.0, -0.5, -0.0105])
    camera_transform = camera.create_multiview_camera(
        index=1, options={
            'loc': camera_pos,
            'rot': Euler([np.pi/2, 0.0, 0.0]),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_transform.name = "tibia_transform_view"

    camera.remove_default_multiview_cameras()

    # Lights

    set_scene_settings(renderer='BLENDER_EEVEE')

    bpy.context.scene.render.resolution_x = 1080
    bpy.context.scene.render.resolution_y = 1920

    #: Add hdri
    add_lights()

    #: Render transparent
    bpy.data.scenes["Tibia"].render.film_transparent = True
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGBA'

    #: Switch camera
    bpy.data.scenes["Tibia"].camera = camera_hbp
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "tibia_hbp_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)

    #: Switch camera
    bpy.data.scenes["Tibia"].camera = camera_osim
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "tibia_osim_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)

    #: Switch camera
    bpy.data.scenes["Tibia"].camera = camera_transform
    # render settings
    bpy.context.scene.render.filepath = os.path.join(
        figures_path, "tibia_transform_mesh_registration.png")
    if render:
        bpy.ops.render.render(write_still=1)


def parse_args():
    """ Parge arguments """
    argv = sys.argv
    argv = argv[argv.index('--') + 1:] if '--' in argv else ''
    parser = ArgumentParser()
    #: render
    parser.add_argument("--render", "-r", required=False, action='store_true',
                        default=False, dest="render")
    return parser.parse_args(argv)


def generate_matplotlib_figure(segment_name: str) -> bool:
    """ Generate matplotlib figure """
    fig, axs = plt.subplots(3, 1)
    axs[0].set_xlabel("")
    axs[0].set_ylabel("")
    axs[0].grid(False)
    axs[0].set_title("Mesh Registration")
    segment_name_hbp = mpimg.imread("../../src/figures/femur_hbp.png")
    segment_name_osim = mpimg.imread("../../src/figures/femur_osim.png")
    axs[0].imshow(segment_name_hbp)
    plt.show()
    return True


if __name__ == '__main__':
    args = parse_args()
    if 'resources' not in bpy.data.scenes.keys():
        import_resources()
    generate_pelvis(render=False)
    generate_femur(render=False)
    generate_tibia(render=False)  # render=args.render
    # generate_matplotlib_figure("femur")
