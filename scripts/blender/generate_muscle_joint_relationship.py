"""
Generate muscle joint relationship based on moment arms
"""

import bisect
import os
import sys
from collections import defaultdict
from pathlib import Path
from pprint import pprint

import h5py
import numpy as np
import tqdm

import bpy
from farms_blender.core.muscles import (compute_moment, compute_moment_arm,
                                        get_muscle_joints)
from farms_blender.core.objects import objs_of_farms_types
from farms_data.io.yaml import read_yaml, write_yaml
from loading import load_scene

SCRIPT_PATH = Path(bpy.data.filepath)
sys.path.append(SCRIPT_PATH)

# Global config paths
CONFIG_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath( "muscle_groups_definition.yaml")
MUSCLE_RESULTS_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "results", "muscle_analysis")


def get_muscle_group(muscle_group_def, joint_name, moment_arm):
    """Get the sign of the moment arm"""
    m_min, m_max = min(moment_arm), max(moment_arm)
    if abs(m_min) < abs(m_max):
        m_sign = np.sign(m_max)
    elif abs(m_min) > abs(m_max):
        m_sign = np.sign(m_min)
    muscle_group = [
        group
        for group, value in muscle_group_def.items()
        if value['joint'] == joint_name.lower() and value['sign'] == m_sign
    ][0]
    return muscle_group


def setup():
    """ Set up the scene """
    return load_scene()


def generate_relationship(model_name, moment_threshold=1e-6):
    """
    For the muscles and joints in the model identify the ones that
    effect a muscle

    The joints are also sorted according to their maximum moment arm values

    Parameters
    ----------
    sdf_name: <pathlib.Path>
        Path like object of the sdf model
    muscle_config_name: <pathlib.Path>
        Path like object of the muscle config file
    export_path: <pathlib.Path>
        Path like object of the export path
    muscle_group_path: <pathlib.Path>
        Path like object of the muscle grouping path
    moment_threshold: <float>
        Threshold to include/exclude muscle influence on a joint

    Returns
    -------
    return: <None>
        No return value
    """
    # Load muscle grouping default convention
    muscle_group_def = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )

    # Load muscle names
    muscle_names = read_yaml(MUSCLE_CONFIG_PATH.joinpath("names.yaml"))

    # Setup dataset
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("muscle_joint_relationship.h5"), "w")

    # Iterate over muscles
    side = "RIGHT"
    for limb in tqdm.tqdm(muscle_names):
        for muscle_name in tqdm.tqdm(limb):
            # Muscle object
            muscle_full_name = f"{side}_{limb}_{muscle_name}"
            muscle_obj = bpy.data.objects[muscle_full_name]
            # Create groups and datasets
            muscle_data_group = data.create_group(f"/{limb}/{muscle_name}/")
            muscle_data_group.attrs['span_joints'] = [
                joint[1:].lower()
                for joint in get_muscle_joints(model_name, f"{muscle_name}")
            ]
            muscle_data_group.attrs['force'] = muscle_obj['f_max']
            for joint_name in muscle_data_group.attrs['span_joints']:
                joint_full_name = f"{side[0]}{joint_name.upper()}"
                joint_angles, moment_arm, moment = compute_moment(
                    muscle_full_name, joint_full_name
                )
            # Use walrus operator once upgraded to 3.8
            moment_value = max(abs(moment_arm))
            if moment_value > moment_threshold:
                joints.append(joint_name)
                moment_values.append(moment_value)
                moment_arms.append(moment_arm)
                muscle_group = get_muscle_group(
                    muscle_group_def, joint_root_name, moment_arm
                )
                joint_muscle_relationship[limb][muscle_group].append(
                    [muscle_root_name, joint_root_name.lower()]
                )
        joints = [
            joint[1:]
            for _, joint in sorted(zip(moment_values, joints), reverse=True)
        ]
        muscle_joint_relationship[limb][muscle_root_name] = joints
    # Restructure
    # Write the data
    if export_path:
        write_yaml(muscle_joint_relationship, export_path)
        write_yaml(
            joint_muscle_relationship,
            "../../data/config/auto_generated_joint_muscle_relationship.yaml"
        )
    # Return the dict
    return muscle_joint_relationship


def main():
    """ Main """
    # Setup scene
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_obj = setup()
    # Generate config files
    generate_config_files()


if __name__ == '__main__':
    generate_relationship(
        "mouse_with_joint_limits", "forelimb", "hindlimb",
        moment_threshold=1e-5,
        export_path="../../data/config/auto_generated_muscle_joint_relationship.yaml"
    )
