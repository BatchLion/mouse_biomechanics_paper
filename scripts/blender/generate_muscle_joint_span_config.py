"""
Generate muscle joint relationship based on moment arms
"""

import bisect
import os
import sys
from collections import defaultdict
from pathlib import Path
from pprint import pprint

import numpy as np
import tqdm

import bpy
from farms_blender.core.muscles import (
    compute_moment, compute_moment_arm, get_muscle_joints
)
from farms_blender.core.objects import objs_of_farms_types
from farms_data.io.yaml import read_yaml, write_yaml
from loading import load_scene

script_path = bpy.data.filepath
sys.path.append(script_path)


# Global config paths
CONFIG_PATH = Path(script_path).joinpath("..", "..", "data", "config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath( "muscle_groups_definition.yaml")


def get_muscle_group(muscle_group_def, joint_name, moment_arm):
    """Get the sign of the moment arm"""
    m_min, m_max = min(moment_arm), max(moment_arm)
    if abs(m_min) < abs(m_max):
        m_sign = np.sign(m_max)
    elif abs(m_min) > abs(m_max):
        m_sign = np.sign(m_min)
    muscle_group = [
        group
        for group, value in muscle_group_def.items()
        if value['joint'] == joint_name.lower() and value['sign'] == m_sign
    ][0]
    return muscle_group


def setup():
    """ Set up the scene """
    return load_scene()



def generate_config_files(model_name):
    """
    For the muscles and joints in the model identify the ones that
    effect a muscle

    The joints are also sorted according to their maximum moment arm values

    Parameters
    ----------
    model_name: <str>
        Name of the model
    muscle_config_name: <pathlib.Path>
        Path like object of the muscle config file
    export_path: <pathlib.Path>
        Path like object of the export path
    muscle_group_path: <pathlib.Path>
        Path like object of the muscle grouping path
    moment_threshold: <float>
        Threshold to include/exclude muscle influence on a joint

    Returns
    -------
    return: <None>
        No return value
    """
    # Load muscle grouping default convention
    muscle_group_def = read_yaml(
        Path(script_path).joinpath(
            "../../data/config/muscles/muscle_groups_definition.yaml"
        )
    )

    # Create empty dataset
    # muscle_joint_relationship = {leg : {} for leg in ('HIND', 'FORE')}
    # muscle_joint_relationship['HIND'] = {
    #     dof : []
    #     for dof in (
    #             'Hip_flexors', 'Hip_extensors', 'Hip_adductors', 'Hip_abductors',
    #             'Hip_ext_rotators', 'Hip_int_rotators', 'Knee_flexors', 'Knee_extensors',
    #             'Ankle_dorsiflexors', 'Ankle_plantarflexors', 'Ankle_everters',
    #             'Ankle_inverters',
    #     )
    # }
    # muscle_joint_relationship['FORE'] = {
    #     dof : []
    #     for dof in (
    #             'Shoulder_flexors', 'Shoulder_extensors', 'Shoulder_adductors',
    #             'Shoulder_abductors', 'Shoulder_ext_rotators',
    #             'Shoulder_int_rotators', 'Elbow_flexors', 'Elbow_extensors',
    #             'Elbow_supinators', 'Elbow_pronators', 'Wrist_flexors',
    #             'Wrist_extensors', 'Wrist_abductors', 'Wrist_adductors',
    #     )
    # }
    muscle_joint_relationship = {'HIND': {}, 'FORE': {}}
    joint_muscle_relationship = {
        'HIND': defaultdict(list), 'FORE': defaultdict(list)
    }
    # Get unique muscles in the set
    muscles = set([
        obj.name # '_'.join(obj.name.split('_')[2:])
        for obj in [*hindlimb_muscle_objs, *forelimb_muscle_objs]
    ])
    # Iterate over muscles
    for muscle_name in tqdm.tqdm(muscles):
        joints = []
        moment_values = []
        moment_arms = []
        for joint_name in get_muscle_joints(model_name, f"{muscle_name}"):
            limb = muscle_name.split('_')[1]
            muscle_root_name = '_'.join(muscle_name.split('_')[2:])
            joint_root_name = joint_name[1:]
            _, moment_arm = compute_moment(muscle_name, joint_name)
            # Use walrus operator once upgraded to 3.8
            moment_value = max(abs(moment_arm))
            if moment_value > moment_threshold:
                joints.append(joint_name)
                moment_values.append(moment_value)
                moment_arms.append(moment_arm)
                muscle_group = get_muscle_group(
                    muscle_group_def, joint_root_name, moment_arm
                )
                joint_muscle_relationship[limb][muscle_group].append(
                    [muscle_root_name, joint_root_name.lower()]
                )
        joints = [
            joint[1:]
            for _, joint in sorted(zip(moment_values, joints), reverse=True)
        ]
        muscle_joint_relationship[limb][muscle_root_name] = joints
    # Restructure
    # Write the data
    if export_path:
        write_yaml(muscle_joint_relationship, export_path)
        write_yaml(
            joint_muscle_relationship,
            "../../data/config/auto_generated_joint_muscle_relationship.yaml"
        )
    # Return the dict
    return muscle_joint_relationship



def generate_relationship(
        sdf_name, hindlimb_muscle_config, forelimb_muscle_config,
        export_path, muscle_group_path=None, moment_threshold=1e-5
):
    """
    For the muscles and joints in the model identify the ones that
    effect a muscle

    The joints are also sorted according to their maximum moment arm values

    Parameters
    ----------
    sdf_name: <pathlib.Path>
        Path like object of the sdf model
    muscle_config_name: <pathlib.Path>
        Path like object of the muscle config file
    export_path: <pathlib.Path>
        Path like object of the export path
    muscle_group_path: <pathlib.Path>
        Path like object of the muscle grouping path
    moment_threshold: <float>
        Threshold to include/exclude muscle influence on a joint

    Returns
    -------
    return: <None>
        No return value
    """
    # Load muscle grouping default convention
    muscle_group_def = read_yaml(
        Path(script_path).joinpath(
            "../../data/config/muscles/muscle_groups_definition.yaml"
        )
    )
    # Load model
    model_name, model_objs = load_model(sdf_name)
    # Load muscles
    hindlimb_muscle_objs = [
        muscle
        for muscle in load_muscles(hindlimb_muscle_config)
    ]
    forelimb_muscle_objs = [
        muscle
        for muscle in load_muscles(forelimb_muscle_config)
    ]
    # Create empty dataset
    # muscle_joint_relationship = {leg : {} for leg in ('HIND', 'FORE')}
    # muscle_joint_relationship['HIND'] = {
    #     dof : []
    #     for dof in (
    #             'Hip_flexors', 'Hip_extensors', 'Hip_adductors', 'Hip_abductors',
    #             'Hip_ext_rotators', 'Hip_int_rotators', 'Knee_flexors', 'Knee_extensors',
    #             'Ankle_dorsiflexors', 'Ankle_plantarflexors', 'Ankle_everters',
    #             'Ankle_inverters',
    #     )
    # }
    # muscle_joint_relationship['FORE'] = {
    #     dof : []
    #     for dof in (
    #             'Shoulder_flexors', 'Shoulder_extensors', 'Shoulder_adductors',
    #             'Shoulder_abductors', 'Shoulder_ext_rotators',
    #             'Shoulder_int_rotators', 'Elbow_flexors', 'Elbow_extensors',
    #             'Elbow_supinators', 'Elbow_pronators', 'Wrist_flexors',
    #             'Wrist_extensors', 'Wrist_abductors', 'Wrist_adductors',
    #     )
    # }
    muscle_joint_relationship = {'HIND': {}, 'FORE': {}}
    joint_muscle_relationship = {
        'HIND': defaultdict(list), 'FORE': defaultdict(list)
    }
    # Get unique muscles in the set
    muscles = set([
        obj.name # '_'.join(obj.name.split('_')[2:])
        for obj in [*hindlimb_muscle_objs, *forelimb_muscle_objs]
    ])
    # Iterate over muscles
    for muscle_name in tqdm.tqdm(muscles):
        joints = []
        moment_values = []
        moment_arms = []
        for joint_name in get_muscle_joints(model_name, f"{muscle_name}"):
            limb = muscle_name.split('_')[1]
            muscle_root_name = '_'.join(muscle_name.split('_')[2:])
            joint_root_name = joint_name[1:]
            _, moment_arm = compute_moment(muscle_name, joint_name)
            # Use walrus operator once upgraded to 3.8
            moment_value = max(abs(moment_arm))
            if moment_value > moment_threshold:
                joints.append(joint_name)
                moment_values.append(moment_value)
                moment_arms.append(moment_arm)
                muscle_group = get_muscle_group(
                    muscle_group_def, joint_root_name, moment_arm
                )
                joint_muscle_relationship[limb][muscle_group].append(
                    [muscle_root_name, joint_root_name.lower()]
                )
        joints = [
            joint[1:]
            for _, joint in sorted(zip(moment_values, joints), reverse=True)
        ]
        muscle_joint_relationship[limb][muscle_root_name] = joints
    # Restructure
    # Write the data
    if export_path:
        write_yaml(muscle_joint_relationship, export_path)
        write_yaml(
            joint_muscle_relationship,
            "../../data/config/muscles/auto_generated_joint_muscle_relationship.yaml"
        )
    # Return the dict
    return muscle_joint_relationship


def main():
    """ Main """
    # Setup scene
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_obj = setup()
    # Generate config files
    generate_config_files()



if __name__ == '__main__':
    generate_relationship(
        "mouse_with_joint_limits", "forelimb", "hindlimb",
        moment_threshold=1e-5,
        export_path="../../data/config/muscles/auto_generated_muscle_joint_relationship.yaml"
    )
