"""Generate data for muscle parameters sensitivity.

Muscle parameters (fmax, lopt, lts, pennation) varied by +5% by default

"""

import os
import pathlib
import sys
import time
from collections import defaultdict

from farms_data.io import io
import farms_pylog as pylog
import h5py

import bpy
import mathutils
from farms_blender.core import muscles, scene, utils
from farms_blender.core.display import display_farms_types

script_path = os.path.abspath(bpy.data.filepath)
sys.path.append(script_path)
from loading import load_model, load_muscles


def create_data_groups(data_file, muscle_joint_set):
    """ Add new muscle group. """
    for limb, muscle_joint in muscle_joint_set.items():
        limb_group = data_file.create_group(name=limb)
        for muscle, joints in muscle_joint.items():
            muscle_group = limb_group.create_group(name=muscle)
            offset_groups = [
                muscle_group.create_group(name=offset)
                for offset in (
                        "original",
                        "f_max",
                        "l_opt",
                        "l_slack",
                        "pennation",
                )
            ]
            joint_groups = [
                offset_group.create_group(name=joint)
                for offset_group in offset_groups
                for joint in joints
            ]


def generate_muscle_data(
        limb_name, muscle_name, joint_names, offset_group, **kwargs
):
    """ Compute muscle-joint data. """
    num_steps = kwargs.pop("num_steps", 25)
    muscle_suffix = "RIGHT_{}_{}"
    joint_suffix = "R{}"
    muscle_full_name = muscle_suffix.format(limb_name, muscle_name)
    for joint_name in joint_names:
        joint_full_name = joint_suffix.format(joint_name)
        pylog.debug(
            "Generating offset data : {} - {}".format(
                muscle_name, joint_full_name
            )
        )
        data = muscles.analyze_muscle(
            muscle_full_name, joint_full_name, primary_num_steps=num_steps
        )
        joint_group = offset_group[joint_name]
        joint_group.attrs['primary_joint_name'] = joint_name
        for key, value in data.items():
            joint_group.create_dataset(name=key, data=value)


def change_muscle_parameter_value(
        muscle_objs: list, param_name: str, factor: float
):
    """ Change the specified muscle parameter value by a factor """
    for muscle in muscle_objs:
        muscle[param_name] = muscle[param_name]*factor


def main():
    """ Main """
    export_path = os.path.join(
        script_path, "..", "..", "data", "results", "muscle_analysis"
    )

    muscles_config_path = os.path.join(
        script_path, "..", "..", "data", "config", "muscles", "{}.yaml"
    )

    config_path = os.path.join(
        script_path, "..", "..", "data", "config", "{}.yaml"
    )

    #: Load model
    utils.clear_world()
    #: Load animat
    model_name, model_objs = load_model("mouse_with_joint_limits")

    #: Load muscle
    forelimb_muscle_config = io.read_yaml(
        muscles_config_path.format("forelimb")
    )

    hindlimb_muscle_config = io.read_yaml(
        muscles_config_path.format("hindlimb")
    )

    muscle_objs = load_muscles(forelimb_muscle_config)
    muscle_objs += load_muscles(hindlimb_muscle_config)

    #: Update display
    #: Elements to display. Can be changed later
    display = { 'view': True, 'render': False, 'link': False,
        'visual': False, 'collision': False, 'inertial': False,
        'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
    }
    display_farms_types(objs=[*model_objs, *muscle_objs], **display)

    #: Read the muscle joint relationship
    joint_muscle_relation = utils.read_yaml(
        config_path.format("joint_muscle_relationship")
    )

    #: Create h5 data-file
    file_path = os.path.join(export_path, "muscle_parameter_sensitivity_data.h5")
    data_file = h5py.File(file_path, 'w')

    #: Generate muscle_joint set
    muscle_joint_set = {
        limb_name : defaultdict(list)
        for limb_name in joint_muscle_relation.keys()
    }

    for limb_name, limb_data in joint_muscle_relation.items():
        for joint_group, joint_data in limb_data.items():
            for muscle_name, joint_name in joint_data:
                muscle_joint_set[limb_name][muscle_name].append(joint_name)

    #: Create data
    create_data_groups(data_file, muscle_joint_set)

    #: offsets
    param_scale = {
        "original" : None,
        "f_max" : 1.05,
        "l_opt" : 1.05,
        "l_slack" : 1.05,
        "pennation" : 1.05,
    }
    #: Loop over the muscles
    for param_name, param_value in param_scale.items():
        if "original" not in  param_name:
            change_muscle_parameter_value(
                muscle_objs, param_name, factor=param_value
        )
        for limb_name, muscle_joint in muscle_joint_set.items():
            for muscle_name, joint_names in muscle_joint.items():
                offset_group = data_file[limb_name][muscle_name][param_name]
                generate_muscle_data(
                    limb_name, muscle_name, joint_names, offset_group
                )
        if "original" not in  param_name:
            change_muscle_parameter_value(
                muscle_objs, param_name, factor=1/param_value
        )
    #: Close h5 file
    data_file.close()


if __name__ == '__main__':
    import time
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
