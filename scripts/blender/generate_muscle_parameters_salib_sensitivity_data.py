"""Generate data for muscle parameters sensitivity.

Muscle parameters (fmax, lopt, lts, pennation) varied by +5% by default

"""

import os
import sys
import time
from collections import defaultdict
from pathlib import Path

import farms_pylog as pylog
import h5py
import numpy as np
from SALib.analyze import fast, sobol
from SALib.sample import fast_sampler, saltelli
from tqdm import tqdm

import bpy
import mathutils
from farms_blender.core import muscles, scene, utils
from farms_blender.core.display import display_farms_types
from farms_data.io.yaml import read_yaml
from loading import load_scene

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath(
    "muscle_groups_definition.yaml")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")

pylog.set_level('debug')


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    utils.clear_world()
    # Load animat
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene()

    # Update display
    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': False,
               'visual': False, 'collision': False, 'inertial': False,
               'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
               }
    display_farms_types(objs=model_objs, **display)
    return model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def generate_muscle_data(
        limb_name, muscle_name, joint_name, **kwargs
):
    """ Compute muscle-joint data. """
    num_steps = kwargs.pop("num_steps", 10)
    muscle_suffix = "RIGHT_{}_{}"
    joint_suffix = "R{}"
    muscle_full_name = muscle_suffix.format(limb_name, muscle_name)
    joint_full_name = joint_suffix.format(joint_name)
    data = muscles.analyze_muscle(
        muscle_full_name, joint_full_name, num_steps=num_steps
    )
    return data['moment'], data['primary_joint_angles']


def change_muscle_parameter_value(
        muscle: bpy.data.objects, param_names: list, factors: list
):
    """ Change the specified muscle parameter value by a factor """
    for param_name, factor in zip(param_names, factors):
        muscle[param_name] = muscle[param_name]*factor
    return None


def compute_rms(signal, axis=-1):
    """Compute the rms value between observed moments for each sample
    and the default moment"""
    rms = np.sqrt(np.mean(np.square(signal), axis=axis))
    assert np.all(rms > 0.0), "Negative rms values!"
    return rms


def main():
    """ Main """
    # Setup scene
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = setup_scene()

    #: Read the muscle analysis data
    main_data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), "r")

    #: Create data
    file_path = MUSCLE_RESULTS_PATH.joinpath(
        "muscle_parameter_salib_sensitivity.h5"
    )
    data_file = h5py.File(file_path, 'w')

    #: Initialize
    num_steps = 15
    problem = {
        'num_vars': 4,
        'names': ['f_max', 'l_opt', 'l_slack', 'pennation'],
        'bounds': [[0.9, 1.1],
                   [0.9, 1.1],
                   [0.9, 1.1],
                   [0.9, 1.1]]
    }
    # Generate samples
    num_samples = 128
    param_values = saltelli.sample(
        problem, num_samples, calc_second_order=False
    )

    #: Loop over the muscles
    for limb_name, muscles in main_data.items():
        pylog.debug(limb_name)
        for muscle_name, muscle_data in muscles.items():
            pylog.debug(muscle_name)
            muscle_obj = bpy.data.objects[f"RIGHT_{limb_name}_{muscle_name}"]
            for joint_name in muscle_data.attrs['active_joints']:
                pylog.debug(
                    f"Computing SA of {limb_name}_{muscle_name}_{joint_name}"
                )
                # Update data
                iter_data = data_file.create_group(
                    f'{limb_name}/{muscle_name}/{joint_name}'
                )
                #: compute default moment
                default_moment, _ = generate_muscle_data(
                    limb_name, muscle_name, joint_name, num_steps=num_steps
                )
                result = np.zeros((num_steps, len(param_values)))
                for j, param in enumerate(param_values):
                    change_muscle_parameter_value(
                        muscle_obj, problem['names'], factors=param
                    )
                    result[:, j], joint_angle = generate_muscle_data(
                        limb_name, muscle_name, joint_name, num_steps=num_steps
                    )
                    change_muscle_parameter_value(
                        muscle_obj, problem['names'], factors=1/param
                    )
                rms = compute_rms(result, axis=0) - compute_rms(default_moment)
                sobol_si = sobol.analyze(
                    problem, rms, print_to_console=False,
                    calc_second_order=False, parallel=True, n_processors=6
                )
                #: Update data
                iter_data.create_dataset('moments', data=result)
                iter_data.create_dataset('parameters', data=param_values)
                iter_data.create_dataset('joint_angles', data=joint_angle)
                iter_data.create_dataset('default_moment', data=default_moment)
                iter_data.create_dataset('rms', data=rms)
                for name, value in sobol_si.items():
                    iter_data.create_dataset(f'sobol_{name}', data=value)


if __name__ == '__main__':
    import time
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
