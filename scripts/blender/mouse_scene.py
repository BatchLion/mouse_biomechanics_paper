""" Setup the scene for mouse model. """

import os
import pathlib

from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.freestyle import (
    configure_lineset, create_lineset, default_lineset_config,
    enable_freestyle, remove_lineset, set_freestyle_setting
)
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.sdf import load_sdf, get_base_link
from farms_blender.core.scene import load_animat, set_scene_settings
from farms_blender.core.utils import clear_world
from farms_models.utils import get_sdf_path

import bpy

script_path = pathlib.Path(__file__).parent.absolute()

#: Paths
models_path = os.path.join(
    script_path, "..", "..", "data", "models"
)
figures_path = os.path.join(
    script_path, "..", "..", "figures"
)


def add_floor(**kwargs):
    """Add floor"""
    floor_offset = kwargs.pop('floor_offset', [-0.05, 0.0, 0.0])
    name, links_map, joints_map, objs = load_sdf(
        get_sdf_path(
            name='arena_flat',
            version='v0',
        ),
        position=kwargs.pop('position', floor_offset),
        texture='BIOROB2_blue.png',
        texture_collision=True,
    )
    scale = kwargs.pop('scale', 1.)
    objs[0].scale = [scale]*3


def add_lights(**kwargs):
    """ Add lights """
    #: Add sun
    sun = bpy.data.lights.new(name="sun", type='SUN')
    sun.energy = kwargs.pop('sun_energy', 4.0)
    sun.angle = kwargs.pop('sun_angle', 3.14)
    sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
    bpy.context.collection.objects.link(sun_obj)


def configure_freestyle(objs=None, **kwargs):
    """ Configure line style settings. """
    #: Line thickness
    bpy.data.scenes['Scene'].render.line_thickness = kwargs.pop(
        'line_thickness', 1.0
    )

    #: Create collections for visual objects
    if objs:
        for obj in objs:
            add_object_to_collection(obj, collection='freestyle', create=True)
    else:
        for obj, _ in objs_of_farms_types(visual=True, link=True):
            add_object_to_collection(obj, collection='freestyle', create=True)

    #: Genric settings
    set_freestyle_setting('crease_angle', 0)
    #: Remove default lineset
    remove_lineset('LineSet')
    #: Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["visuals",]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = kwargs.pop(
        "select_by_visibility", True
    )
    lineset_config['select_by_collection'] = kwargs.pop(
        "select_by_collection", True
    )
    lineset_config['select_contour'] = kwargs.pop(
        "select_contour", True
    )
    lineset_config['select_silhouette'] = kwargs.pop(
        "select_silhouette", False
    )
    lineset_config['select_crease'] = kwargs.pop(
        "select_crease", False
    )
    lineset_config["select_border"] = kwargs.pop(
        "select_border", False
    )
    lineset_config['select_external_contour'] = kwargs.pop(
        "select_external_contour", True
    )

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections['freestyle']
        configure_lineset(lineset, **lineset_config)


def scene(**kwargs):
    """ scene """
    if kwargs.pop("clear_world", True):
        #: Clear world
        clear_world()
    if kwargs.pop("add_lights", True):
        #: Add light
        add_lights(**kwargs)
    #: Set render settings
    set_scene_settings(renderer='BLENDER_EEVEE')
    #: background light
    bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 1.0
    #: Render size
    bpy.context.scene.render.resolution_x = kwargs.pop(
        "resolution_x", 1920
    )
    bpy.context.scene.render.resolution_y = kwargs.pop(
        "resolution_y", 1080
    )
    #: Render samples
    bpy.context.scene.eevee.taa_render_samples = kwargs.pop(
        "render_samples", 64
    )
    #: Disable background
    bpy.data.scenes['Scene'].render.film_transparent = kwargs.pop(
        "film_transparent", True
    )
    #: Enable freestyle and configure freestyle
    if kwargs.pop("freestyle", False):
        enable_freestyle()


def auto_crop_image(image_path):
    """
    Auto crop image with imagemagick tool
    """
    os.system(f"convert {image_path} -trim +repage {image_path}")


def set_model_offset(model_name, offset_position, offset_orientation=None):
    """Set model offset"""

    #: Set model offset
    base_link = get_base_link(model_name)
    base_link.location = offset_position
    if offset_orientation:
        base_link.rotation_euler = offset_orientation


if __name__ == '__main__':
    scene()
