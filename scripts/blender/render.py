""" Generate blender scene for rendering images. """
import numpy as np

from farms_blender.core import scene
from farms_blender.core import camera

scene.load_scene(animation=False)

#: Add cameras
camera1 = camera.create_multiview_camera(
    index=1, options={
        'loc': np.zeros(3),
        'rot': np.zeros(3),
        'type': 'ORTHO',
        'lens': 50,
    }
)

camera.remove_default_multiview_cameras()
