""" Optimize hindlimb muscle parameters. """

from copy import deepcopy
import sys
import os
import pathlib

import h5py
import numpy as np
import matplotlib.pyplot as plt
import yaml

import bpy
sys.path.append(bpy.data.filepath)
from mathutils import Matrix, Vector, Euler

from farms_blender.core import muscles, sdf
from farms_blender.core.display import display_farms_types
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.utils import read_yaml, write_yaml
from farms_blender.core.pose import set_model_pose, reset_model_pose_to_zero
from farms_blender.core.muscles import compute_current_muscle_tendon_length

import mouse_scene

SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()

MUSCLES_CONFIG_PATH = os.path.join(
        SCRIPT_PATH, "..", "..", "data", "config", "{}.yaml"
)

DATA_PATH = os.path.join(
    SCRIPT_PATH, "..", "..", "data", "results", "muscle_analysis"
)

def generate_hindlimb(**kwargs):
    """ Generate hindlimb """
    model_offset = kwargs.pop('model_offset', (0.0, 0.0, 0.0))
    #: Load hindlimb sdf
    model_name, *_, objs = sdf.load_sdf(
        "../../data/models/sdf/right_hindlimb.sdf",
        resources_scale=5e-3
    )

    #: Load muscle
    muscles.load_muscles(
        muscle_config=MUSCLES_CONFIG_PATH.format("right_hindlimb_muscles"),
        muscle_radius=1e-4, attachment_radius=4e-4
    )

    #: Set model offset
    base_link = sdf.get_base_link(model_name)
    base_link.location = model_offset

    #: Display
    display = {'view': True, 'render': True, 'link': False, 'visual': True,
        'collision': False, 'inertial': False, 'com': False, 'muscle': True,
        'joint': False, 'joint_axis': False,
    }
    display_farms_types(objs=objs, **display)
    return model_name


def get_muscle_length_for_poses(model_name, muscle_name, poses):
    """ Get muscle length for given list of poses """
    joints = list(poses.keys())
    num_poses = len(poses[joints[0]])
    muscle_lengths = np.zeros((num_poses,))
    for j in range(num_poses):
        pose = {"R"+key : value[j] for key, value in poses.items()}
        set_model_pose(pose, units='radians')
        muscle_lengths[j] = compute_current_muscle_tendon_length(
            muscle_name=muscle_name
        )
    #: Reset model to zero pose
    reset_model_pose_to_zero(model_name)
    return muscle_lengths


def optimize():
    """Optimize hindlimb parameters"""
    #: Load the hindlimb model
    model_name = generate_hindlimb()
    #: Load opensim data
    data = h5py.File(
        os.path.join(DATA_PATH, "mouse_hindlimb_osim_for_scaling.h5"), 'r'
    )
    #: Load original muscle config file
    muscle_config = read_yaml(MUSCLES_CONFIG_PATH.format("right_hindlimb_muscles"))
    #: Copy for scaled config
    scaled_muscle_config = {"muscles": {}}
    #: Muscle to optimize for
    for muscle_name, muscle_data in data.items():
        #: Muscle properties
        l_opt = muscle_data.attrs.get("optimal_fiber_length")
        l_slack = muscle_data.attrs.get("tendon_slack_length")
        #: Unscaled data
        unscaled_data = {}
        unscaled_data["l_mtu"] = np.asarray(
            muscle_data["muscle_tendon_length"]
        )
        unscaled_data["l_m_norm"] = np.asarray(
            muscle_data["fiber_length"]
        )/l_opt
        unscaled_data["l_t_norm"] = np.asarray(
                muscle_data["tendon_length"]
        )/l_slack
        unscaled_data["pennation_angle"] = np.asarray(
            muscle_data["pennation_angle"]
        )
        scaled_data = {
            "l_mtu":
            get_muscle_length_for_poses(
                model_name, "RIGHT_HIND_"+muscle_name,
                muscle_data['joint_angles']
            )
        }
        #: Optimize
        res = muscles.scale_muscle_parameters(
            scaled_data,
            unscaled_data,
            x0=(l_opt, l_slack),
            bounds=([0.1*l_opt, 0.1*l_slack], [2*l_opt, 2*l_slack])
        )
        print(
            f"{muscle_name}--{l_opt}:{res['x'][0]}--{l_slack}:{res['x'][1]}"
        )
        print(res)
        plt.plot(unscaled_data["l_mtu"], 'r')
        plt.plot(
            unscaled_data["l_m_norm"]*l_opt*np.cos(unscaled_data["pennation_angle"])+unscaled_data["l_t_norm"]*l_slack,
        '-b')
        plt.grid(True)
        plt.show()
        break
        #: Update config
        scaled_muscle_config["muscles"][f"RIGHT_HIND_{muscle_name}"] = (
            deepcopy(muscle_config["muscles"][f"RIGHT_HIND_{muscle_name}"])
        )
        config = scaled_muscle_config["muscles"][f"RIGHT_HIND_{muscle_name}"]
        config["l_opt"] = float(res['x'][0])
        config["l_slack"] = float(res['x'][1])
    #: Export config
    write_yaml(
        scaled_muscle_config,
        MUSCLES_CONFIG_PATH.format("right_hindlimb_muscles_scaled")
    )


if __name__ == '__main__':
    optimize()
