from pathlib import Path

import h5py

from farms_data.io.yaml import read_yaml, write_yaml

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")
FIGURE_PATH = SCRIPT_PATH.joinpath("..", "src", "figures")
FIGURE_DOF_PATH = FIGURE_PATH.joinpath("dof")


def main():
    """ Main """

    # muscle names
    muscle_names = read_yaml(MUSCLE_CONFIG_PATH.joinpath("names.yaml"))

    # Muscle ordered list
    muscle_group_by_span = {
        'HIND':{
            'Hip': [],
            'Hip_Knee':[],
            'Knee':[],
            'Ankle_Knee':[],
            'Ankle':[],
        },
        'FORE':{
            'Shoulder': [],
            'Elbow_Shoulder':[],
            'Elbow':[],
            'Elbow_Wrist':[],
            'Wrist':[],
        },
    }

    # read the data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')

    # Sort
    for side, muscles in muscle_names.items():
        for muscle in muscles:
            muscle_data = data[f"/{side}/{muscle}"]
            spans = '_'.join(
                dict.fromkeys([
                    joint.split('_')[0]
                    for joint in  muscle_data.keys()
                ]).keys())
            muscle_group_by_span[side][spans].append(muscle)

    # Export
    write_yaml(
        muscle_group_by_span, MUSCLE_CONFIG_PATH.joinpath("names_by_dof.yaml")
    )

    # close data file
    data.close()



if __name__ == '__main__':
    main()
