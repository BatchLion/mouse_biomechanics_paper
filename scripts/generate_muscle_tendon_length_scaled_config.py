""" Update the properties of length parameters imported from opensim to the 
new mouse model """

import argparse
from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
import farms_pylog as pylog

#: Args
parser = argparse.ArgumentParser()
parser.add_argument("--plot", "-p", type=bool, dest="plot", default=False)
args = parser.parse_args()
show_plot = args.plot

#: Read the hind limb muscle config files
with open(
        "../data/config/fixed_hindlimb_muscle_config.yaml/",
        "r"
) as stream:
    muscle_config = yaml.load(stream, yaml.SafeLoader)
#: Read the hind limb muscle joint relationship config
with open(
        "../data/hind_limb_muscle_tendon_lengths/muscle_joint_names.yaml",
        "r"
) as stream:
    muscle_joint = yaml.load(stream, yaml.SafeLoader)
#: Setup for exporting muscle scaling factors
muscle_scale_factor = {}
muscle_scale_config = "../data/hind_limb_muscle_tendon_lengths/muscle_scale.yaml"
scaled_muscle_config = deepcopy(muscle_config)
#: File name templates to read data
file_path = ("../data/hind_limb_muscle_tendon_lengths/"
             "muscle_tendon_length_{}/{}_{}_muscle_tendon_length.h5")
for name, muscle in muscle_config["muscles"].items():
    #: Muscle name
    muscle_name = name.split("_", maxsplit=2)[-1]
    #: Joint name
    joint_name = muscle_joint[muscle_name]
    #: Read the corresponding opensim muscle-length-joint file
    muscle_length_osim = pd.read_hdf(
        file_path.format("opensim", muscle_name, joint_name)
    )
    #: Read the corresponding pybullet muscle-length-joint file
    muscle_length_bullet = pd.read_hdf(
        file_path.format("pybullet", muscle_name, joint_name)
    )
    if "flexion" not in joint_name:
        muscle_length_bullet[joint_name] *= 1
    #: Get the zero angle length
    osim_zero_length = np.interp(
        0.0, muscle_length_osim[joint_name], muscle_length_osim[muscle_name]
    )
    bullet_zero_length = np.interp(
        0.0, muscle_length_bullet[joint_name], muscle_length_bullet[muscle_name]
    )
    #: Compute scaling factor
    scale_factor = bullet_zero_length/osim_zero_length
    #: Record the scaling factor
    muscle_scale_factor[muscle_name] = scale_factor.tolist()
    #: Scale the length related properties in the system
    scaled_muscle_config["muscles"][name]["l_ce0"] = (
        muscle["l_ce0"]*scale_factor.tolist()
    )
    scaled_muscle_config["muscles"][name]["l_opt"] = (
        muscle["l_opt"]*scale_factor.tolist()
    )
    scaled_muscle_config["muscles"][name]["l_slack"] = (
        muscle["l_slack"]*scale_factor.tolist()
    )
    #: Plotting
    pylog.info("{}-{}".format(muscle_name, scale_factor))
    if show_plot:
        plt.figure()
        plt.title("{} - {}".format(joint_name, muscle_name))
        plt.plot(
            muscle_length_osim[joint_name],
            muscle_length_osim[muscle_name]*scale_factor
        )
        plt.plot(
            muscle_length_bullet[joint_name],
            muscle_length_bullet[muscle_name]
        )
        plt.xlabel("Joint Range of Motion [rad]")
        plt.ylabel("Muscle length [m]")
        plt.grid(True)
        plt.legend(('osim', 'bullet'))
        plt.show()
#: Export the scale factor
with open(muscle_scale_config, "w") as stream:
    to_write = yaml.dump(
        muscle_scale_factor, default_flow_style=False,
        explicit_start=True, indent=4, width=80
    )
    stream.write(to_write)
#: Export new scaled config of muscles
with open(
        "../data/hind_limb_muscle_tendon_lengths/hind_muscles_scaled.yaml",
        "w"
) as stream:
    to_write = yaml.dump(
        scaled_muscle_config, default_flow_style=False,
        explicit_start=True, indent=4, width=80
    )
    stream.write(to_write)
