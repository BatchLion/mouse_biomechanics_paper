""" Generate hill muscle force-length curve """

import os

import farms_pylog as pylog
import matplotlib.pyplot as plt
import numpy as np
import yaml

#: Load default plot params
with open('../data/config/plotting.yaml', 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)

plot_params['text.usetex'] = False
#: Set plot properties
plt.rcParams.update(plot_params)

FILE_PATH = os.path.dirname(__file__)
FIGURES_PATH = os.path.join(FILE_PATH, '../figures')

def save_figure(figure_handle, file_path, file_name, **kwargs):
    """Save figure

    Parameters
    ----------
    figure_handle : 

    name : 

    **kwargs : 


    Returns
    -------
    out : 

    """
    if kwargs.pop('pdf', True):
        figure_handle.savefig(
            os.path.join(file_path, file_name+'.pdf'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight')
        )
    if kwargs.pop('png', True):
        figure_handle.savefig(
            os.path.join(file_path, file_name+'.png'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight')
        )

def plot_degroote_force_length_curve(**kwargs):
    """ Generate force-length curve from degroote formulation

    Parameters
    ----------
    **kwargs : <dict>

    Returns
    -------
    out :

    """
    #: Force-Length Constants
    b1 = kwargs.pop('b1', [0.815, 0.433, 0.1])
    b2 = kwargs.pop('b2', [1.055, 0.717, 1.0])
    b3 = kwargs.pop('b3', [0.162, -0.030, 0.354])
    b4 = kwargs.pop('b4', [0.063, 0.2, 0.0])

    kpe = kwargs.pop('kpe', 4.0)
    e0 = kwargs.pop('e0', 0.6)

    l_ce = np.linspace(0.4, 1.6, 100)
    active_force = np.zeros(np.shape(l_ce))
    passive_force = np.zeros(np.shape(l_ce))
    for step, _l_ce in enumerate(l_ce):
        _active_force = 0.0
        for j in range(3):
            _num = -0.5*(_l_ce - b2[j])**2
            _den = (b3[j] + b4[j]*_l_ce)**2
            _active_force += b1[j]*np.exp(_num/_den)
        active_force[step] = _active_force
        passive_force[step] = (
            np.exp(kpe*(_l_ce - 1)/e0) - 1.0)/(np.exp(kpe) - 1.0) if _l_ce > 1.0 else 0.0

    fig, ax = plt.subplots()
    ax.plot(l_ce, active_force)
    ax.plot(l_ce, passive_force)
    ax.plot(l_ce, active_force+passive_force, ':')
    ax.set_xlabel('$l̃_{ce}$')
    ax.set_ylabel('$f_{act}$')
    ax.set_title('Force-Length Curve')
    ax.legend(('Active force', 'Passive force', 'Total force'))
    ax.grid(True)
    save_figure(fig, FIGURES_PATH, "force_length_curve")

    return l_ce, active_force, passive_force


def plot_degroote_force_velocity_curve(**kwargs):
    """ Generate force-velocity curve from degroote formulation

    Parameters
    ----------
    **kwargs : <dict>

    Returns
    -------
    out :

    """
    #: Force-Velocity Constants
    d1 = kwargs.pop('d1', -0.318)
    d2 = kwargs.pop('d2', -8.149)
    d3 = kwargs.pop('d3', -0.374)
    d4 = kwargs.pop('d4', 0.886)

    v_ce = np.linspace(-1.0, 1.0, 100)
    force = np.zeros(np.shape(v_ce))
    for step, _v_ce in enumerate(v_ce):
        exp1 = d2*_v_ce + d3
        exp2 = ((d2*_v_ce + d3)**2) + 1.
        force[step] = d1*np.log(exp1 + np.sqrt(exp2)) + d4

    fig, ax = plt.subplots()
    ax.plot(v_ce, force)
    ax.set_xlabel('$ṽ_{ce}$')
    ax.set_ylabel('$force$')
    ax.set_title('Force-Velocity Curve')
    ax.grid(True)
    save_figure(fig, FIGURES_PATH, "force_velocity_curve")

    return v_ce, force

if __name__ == '__main__':
    plot_degroote_force_length_curve()
    plot_degroote_force_velocity_curve()
