""" Methods to extract data from osim file. """

import h5py
import numpy as np

from farms_opensim import opensim_utils as osim_utils
import farms_pylog as pylog


def extract_muscle_data(osim_filepath, export_path, muscle_joint):
    """ extract muscle data """
    #: Create h5 data file
    data_file = h5py.File(export_path, 'w')
    #: Initialize the model
    model, state = osim_utils.setup_osim_model(osim_filepath)
    #: Extact muscle properties of all muscles in the model
    muscle_properties = osim_utils.extract_muscle_properties(model)
    #: Extract coordinate properties
    coordinate_properties = osim_utils.extract_static_coordinate_properties(model)
    joints_range = {
        joint['name'] : joint['range']
        for coord_name, coord in coordinate_properties.items()
        for joint in coord
    }
    #: Loop over muscle-joint
    for muscle, joints in muscle_joint:
        for joint in joints:
            pylog.debug(f"Exporting {muscle}-{joint}")
            #: Reset model pose
            osim_utils.reset_model_to_zero_pose(model, state)
            #: Create muscle group
            if muscle not in data_file.keys():
                muscle_group = data_file.create_group(name=muscle)
                #: Add attributes Muscle properties
                for key,value in muscle_properties[muscle].items():
                    muscle_group.attrs.create(key, value)
            else:
                muscle_group = data_file[muscle]
            #: Create joint group
            joint_group = muscle_group.create_group(name=joint)
            #: Get joint properties
            joint_range = joints_range[joint]
            #: Calculate data
            muscle_states = osim_utils.calculate_muscle_states_for_coordinate_range(
                model, muscle, as_a_function_coordinate=joint,
                coordinate_range=np.linspace(joint_range[0], joint_range[1], 20),
                actuation=1.0
            )
            for key, value in muscle_states.items():
                if isinstance(value, dict):
                    subgroup = joint_group.create_group(name=key)
                    for subkey, subvalue in value.items():
                        subgroup.create_dataset(name=subkey, data=subvalue)
                else:
                    joint_group.create_dataset(name=key, data=value)
