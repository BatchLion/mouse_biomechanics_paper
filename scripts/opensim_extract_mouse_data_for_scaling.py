import os

import h5py
import yaml
import numpy as np

from farms_opensim import opensim_utils as osim_utils


def generate_pose_data_for_scaling(
        osim_filepath, muscle_joint, **kwargs
):
    """ Generate pose data for scaling """
    #: Kwargs
    num_points = kwargs.pop("num_points", 10)
    #: Create data file
    data = {}
    #: Initialize the model
    model, state = osim_utils.setup_osim_model(osim_filepath)
    #: Extract coordinate properties
    coordinate_properties = osim_utils.extract_static_coordinate_properties(
        model)
    joints_range = {
        joint['name']: joint['range']
        for coord_name, coord in coordinate_properties.items()
        for joint in coord
    }
    for muscle_name, joints in muscle_joint["HINDLIMB"].items():
        muscle_data = {}
        coords = np.meshgrid(
            *[
                np.linspace(
                    joints_range[joint][0],
                    joints_range[joint][1],
                    num_points
                )
                for joint in joints
            ]
        )
        for j in range(len(joints)):
            muscle_data[joints[j]] = coords[j].flatten()
        data[muscle_name] = muscle_data
    return data


def extract_muscle_data(osim_filepath, export_path, poses):
    """ extract muscle data """
    #: Create h5 data file
    data_file = h5py.File(export_path, 'w')
    #: Initialize the model
    model, state = osim_utils.setup_osim_model(osim_filepath)
    #: Extact muscle properties of all muscles in the model
    muscle_properties = osim_utils.extract_muscle_properties(model)
    #: Loop over the pose data
    for muscle_name, data in poses.items():
        #: Reset model pose
        osim_utils.reset_model_to_zero_pose(model, state)
        muscle_group = data_file.create_group(name=muscle_name)
        #: Add attributes Muscle properties
        for key, value in muscle_properties[muscle_name].items():
            muscle_group.attrs.create(key, value)
        #: Calculate data
        joints = list(data.keys())
        num_values = len(data[joints[0]])
        muscle_states = osim_utils.create_default_muscle_states_dict(
            num_values, joints
        )
        for joint in joints:
            muscle_states["joint_angles"][joint][:] = poses[muscle_name][joint]
        # Set model pose
        for j in range(num_values):
            pose = {key: value[j] for key, value in data.items()}
            osim_utils.update_model_pose(model, state, pose)
            result = osim_utils.calculate_current_muscle_states(
                model, muscle_name, joints
            )
            for key, value in result.items():
                if "moment" in key:
                    for joint in joints:
                        muscle_states[key][joint][j] = value[joint]
                else:
                    muscle_states[key][j] = value
        for key, value in muscle_states.items():
            if "moment" in key or "joint_angles" in key:
                group = muscle_group.create_group(name=key)
                for sub_key, sub_value in value.items():
                    group.create_dataset(name=sub_key, data=sub_value)
            else:
                muscle_group.create_dataset(name=key, data=value)
    #: Close data
    data_file.close()


if __name__ == '__main__':
    osim_filepath = "../data/models/mouse_hindlimb_2018/Mouse_hindlimb_model_2018_v41.osim"
    export_path = "../data/results/muscle_analysis/mouse_hindlimb_osim_for_scaling.h5"
    with open("../data/config/muscle_joint_relationship.yaml", "r") as stream:
        muscle_joint = yaml.load(stream, yaml.FullLoader)
    muscle_joint = {
        key: value for key, value in muscle_joint.items()
        if "HIND" in key
    }
    poses = generate_pose_data_for_scaling(osim_filepath, muscle_joint)
    extract_muscle_data(
        osim_filepath, export_path, poses
    )
