from copy import deepcopy
import yaml


def get_link(name, data):
    """ get link """
    for link in data:
        if name == link['child_link']:
            return link


def get_link_index(name, data):
    """ get link index"""
    for j, link in enumerate(data):
        if name == link['child_link']:
            return j


with open('./default_pose.yaml', 'r') as stream:
    new_data = yaml.load(stream, yaml.SafeLoader)

with open('./original_pose.yaml', 'r') as stream:
    original_data = yaml.load(stream, yaml.SafeLoader)

links = ['Tail{}'.format(j) for j in range(1, 24)]

for link in links:
    angle = original_data[link][0]
    new_data[link+'_flexion'] = -1*angle

#: write yaml
with open('./default_pose_new.yaml', 'w') as file:
    to_write = yaml.dump(
        new_data,  # default_flow_style=None,
        # explicit_start=True, indent=2, width=80
    )
    file.write(to_write)
