"""Remove wrist inversion from h5 files"""

import farms_pylog as pylog
import h5py

def remove_wrist_inversion(file_path):
    """Remove wrist inversion from data"""
    pylog.info(f"Reading {file_path}")
    data = h5py.File(file_path, "a")
    # Loop and remove data
    for _, side_data in data.items():
        for muscle, muscle_data in side_data.items():
            if "Wrist_inversion" in muscle_data:
                pylog.info(f"Removing wrist inversion from {muscle}")
                del muscle_data["Wrist_inversion"]
    # Close file
    data.close()

# Remove for sensitivity data
remove_wrist_inversion(
    "../../data/results/muscle_analysis/muscle_attachment_salib_0.5mm_sensitivity.h5",
)
remove_wrist_inversion(
    "../../data/results/muscle_analysis/muscle_attachment_salib_1.0mm_sensitivity.h5",
)
remove_wrist_inversion(
    "../../data/results/muscle_analysis/muscle_parameter_salib_sensitivity.h5",
)
