"""Generate Mouse Template"""

import os
import numpy as np
from farms_sdf.sdf import ModelSDF, Link, Joint, Inertial
from farms_sdf.units import SimulationUnitScaling
from farms_sdf import utils
import yaml
import farms_pylog as pylog

MESH_PATH = os.path.join("../meshes/stl", "")
MODEL_NAME = "mouse"


def add_planar_constraint(model, units, joint_type='prismatic'):
    """ Add planar constraints to the model. """
    #: Add planar joints
    world_link = Link(
        name='world',
        pose=np.zeros((6,)),
        visual=None,
        collision=None,
        inertial=Inertial.empty(units),
        units=units
    )
    support_link_1 = Link(
        name='prismatic_support_1',
        pose=np.zeros((6,)),
        visual=None,
        collision=None,
        inertial=Inertial.empty(units),
        units=units
    )
    support_link_2 = Link(
        name='prismatic_support_2',
        pose=np.zeros((6,)),
        visual=None,
        collision=None,
        inertial=Inertial.empty(units),
        units=units
    )
    prismatic_1 = Joint(
        name='prismatic_support_1',
        joint_type=joint_type,
        parent=world_link,
        child=support_link_1,
        xyz=[0.0, 1.0, 0.0],
        limits=[1, -1, 0, 100]
    )
    prismatic_2 = Joint(
        name='prismatic_support_2',
        joint_type=joint_type,
        parent=support_link_1,
        child=support_link_2,
        xyz=[0.0, 0.0, 1.0],
        limits=[1, -1, 0, 100]
    )
    root = utils.find_root(model)
    link_name_id = utils.link_name_to_index(model)
    revolute_1 = Joint(
        name='revolute_support_1',
        joint_type='continuous' if joint_type != 'fixed' else joint_type,
        parent=support_link_2,
        child=model.links[link_name_id[root]],
        xyz=[1.0, 0.0, 0.0],
    )

    model.links.append(world_link)
    model.links.append(support_link_1)
    model.links.append(support_link_2)
    model.joints.append(prismatic_1)
    model.joints.append(prismatic_2)
    model.joints.append(revolute_1)


def main():
    """Main"""
    #: Read the config file
    with open("../../config/gen_sdf_3dof.yaml", 'r') as f:
        data = yaml.safe_load(f)

    joint_limits = [-np.pi, np.pi, 1e10, 2*np.pi*100]
    scale = 1
    units = SimulationUnitScaling()
    #: Generate links
    links = {}
    for elem in data:
        name = elem['child_link']
        LINK_FROM_MESH = True if "interim" not in name else False
        if LINK_FROM_MESH:
            pylog.debug("Mesh link : {}".format(name))
            links[name] = Link.from_mesh(
                name=name,
                mesh=os.path.join(MESH_PATH, elem['mesh_name']),
                pose=np.concatenate([
                    elem['link_location'],
                    np.zeros(3)
                ]),
                shape_pose=np.zeros(6),
                scale=scale,
                units=units,
                inertial_from_bounding=True,
            )
        else:
            links[name] = Link.empty(
                name=name,
                pose=np.concatenate([
                    elem['link_location'],
                    np.zeros(3)
                ]),
                units=units,
            )
    #: Generate joints
    joints = {}
    for elem in data:
        #: Skip for base link
        if elem['parent_link'] is None:
            continue
        name = elem['joint_name']
        joints[name] = Joint(
            name="{}".format(name),
            joint_type="revolute",
            parent=links[elem['parent_link']],
            child=links[elem['child_link']],
            xyz=elem['joint_axis'],
            limits=joint_limits
        )

    sdf = ModelSDF(
        name=MODEL_NAME,
        pose=np.zeros(6),
        links=links.values(),
        joints=joints.values(),
        units=units
    )
    sdf.write(filename="{}.sdf".format(MODEL_NAME))


def edit_model(model):
    """ Edit the generated sdf model. """

    units = SimulationUnitScaling(
        meters=1e0,
        seconds=1e0,
        kilograms=1e0
    )

    link_index = utils.link_name_to_index(model)
    joint_index = utils.joint_name_to_index(model)

    #: Change joint limits:
    limb_joint_limits = {
        'Hip_flexion': [-50, 50], 'Hip_adduction': [-40, 20],
        'Hip_rotation': [-10, 30], 'Knee_flexion': [-145, -20],
        'Ankle_flexion': [-50, 50], 'Ankle_inversion': [-10, 30],
    }
    for name, value in limb_joint_limits.items():
        ljoint = model.joints[joint_index['L'+name]]
        rjoint = model.joints[joint_index['R'+name]]
        ljoint.axis.limits[0] = np.deg2rad(value[0])
        ljoint.axis.limits[1] = np.deg2rad(value[1])
        rjoint.axis.limits[0] = np.deg2rad(value[0])
        rjoint.axis.limits[1] = np.deg2rad(value[1])

    #: Fix foot joints
    for joint, index in joint_index.items():
        if ("Metatarsus" in joint) or ("Phalange" in joint):
            model.joints[index].type = "fixed"

    #: Invert joint axis for adbuction and adduction to match OpenSim
    for joint, index in joint_index.items():
        if ("adduction" in joint) or ("inversion" in joint):
            model.joints[index].axis.xyz = [0.0, -1.0, 0.0]
        elif ("rotation" in joint):
            model.joints[index].axis.xyz = [0.0, 0.0, -1.0]

    add_planar_constraint(model, units, 'fixed')

    ########## WRITE ##########
    model.units = units
    model.write(filename="{}.sdf".format(MODEL_NAME))


if __name__ == '__main__':
    main()
    model = ModelSDF.read('./{}.sdf'.format(MODEL_NAME))[0]
    edit_model(model)
