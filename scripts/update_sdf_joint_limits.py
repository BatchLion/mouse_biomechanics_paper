#!/usr/bin/env python

""" Update the joint limits for the sdf file. """
import os

from argparse import ArgumentParser
import numpy as np

from farms_sdf.sdf import ModelSDF
from farms_sdf import utils
from gen_joint_limits_config import joint_limits


def edit_model(sdf_path, export_path):
    """ Edit the generated sdf model. """
    model = ModelSDF.read(args.sdf_path)[0]

    link_index = utils.link_name_to_index(model)
    joint_index = utils.joint_name_to_index(model)
    #: Change joint limits:
    for joint in model.joints:
        if joint.name in joint_limits:
            joint.axis.limits[:2] = np.deg2rad(joint_limits.get(joint.name))

    ########## WRITE ##########
    if export_path:
        model.write(export_path)
    else:
        model.write(sdf_path)


if __name__ == '__main__':
    parser = ArgumentParser("sdf joint limits")
    parser.add_argument(
        "--sdf-path", "-s", dest="sdf_path", required=True, type=str
    )
    parser.add_argument(
        "--export-path", "-e", dest="export_path", required=False, type=str,
        default=None
    )
    args = parser.parse_args()

    edit_model(args.sdf_path, args.export_path)
